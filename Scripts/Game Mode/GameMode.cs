﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class GameMode 
{
    // Game Mode Variables
    private int time_left = 60;
    private int player_score = 0;

    // User Interface Content
    private GameObject timeleft;
    private GameObject score;

    public GameMode()
    {
        timeleft = GameObject.Find("TimeLeftText");
        score = GameObject.Find("ScoreText");
    }

    public void IncreaseScore() { player_score = player_score + 5; }
    public void IncreaseTimeLeft() { time_left = time_left + 5; }

    public int GetScore() { return player_score; }
    public int GetTimeLeft() { return time_left; }

    public void UpdateGame()
    {
        if(timeleft != null && score != null)
        {
            timeleft.GetComponent<Text>().text = time_left.ToString();
            score.GetComponent<Text>().text = player_score.ToString();
        }
    }

    public IEnumerator CountdownCo()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);
            time_left--;
        }
    }
}
