﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundLibrary : MonoBehaviour
{
    [Header("Audio Sources In The Game")]
    public AudioSource POWERUP;
    public AudioSource SHOOT;
    public AudioSource EXPLOSION;

    [Header("Audio Clips For the Audio Sources")]
    public AudioClip powerupClip;
    public AudioClip laserClip;
    public AudioClip explosionClip;

    // Start is called before the first frame update
    void Start()
    {
        POWERUP.clip = powerupClip;
        SHOOT.clip = laserClip;
        EXPLOSION.clip = explosionClip;
    }
}
