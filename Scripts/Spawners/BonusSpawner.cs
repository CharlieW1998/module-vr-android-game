﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusSpawner : MonoBehaviour
{
    private float timer;
    public GameObject prefab;

    // Start is called before the first frame update
    void Start()
    {
        timer = 8f;
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;

        Vector3 bonusPos = new Vector3(Random.Range(-13f, 13f), 1.6f, Random.Range(-13.5f, 5.0f));

        if (timer <= 0f)
        {
            Instantiate(prefab, bonusPos, Quaternion.identity);

            timer = 8f;
            timer -= Time.deltaTime;
        }
    }
}
