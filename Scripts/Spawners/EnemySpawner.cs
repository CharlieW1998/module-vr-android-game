﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    private float timer;
    public GameObject enemyPrefab;

    // Start is called before the first frame update
    void Start()
    {
        timer = 1f;
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;

        Vector3 enemyPos = new Vector3(Random.Range(-8f, 7f), 1.6f, Random.Range(-13.5f, 2.86f));

        if(timer <= 0f)
        {
            Instantiate(enemyPrefab, enemyPos, Quaternion.identity);

            timer = 2f;
            timer -= Time.deltaTime;
        }

    }
}
