﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// ABSTRACT Class
public class Actor : MonoBehaviour
{
    // actor data
    protected float actor_speed; // an actor has some speed
    protected int actor_health; // an actor has some health / durability
    protected int actor_takendamage; // how much damage can this actor take?

    // Get the health of the actor
    public int GetHealth() { return actor_health; }

    // get the speed of the actor
    public float GetSpeed() { return actor_speed; }

    // amount the amount of damage the actor can take
    public void SetDamage(int amt)
    {
        actor_takendamage = amt;
    }

    // Reduce the actors health
    public void NegateHealth()
    {
        actor_health = actor_health - actor_takendamage;
    }

    // Initialise the actors data
    public void InitialiseActor(float speed, int health, int dmg)
    {
        actor_speed = speed;
        actor_health = health;
        actor_takendamage = dmg;
    }
}
