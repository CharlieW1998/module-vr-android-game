﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Actor
{
    // the target in which the enemy goes to
    private GameObject target;

    // Start is called before the first frame update
    void Start()
    {
        // get the target from the scene
        target = GameObject.Find("Main Camera");

        // Initialise the actor
        InitialiseActor(1.0f, 10, 5);
    }

    // Update is called once per frame
    void Update()
    {
        // make the enemy look at the target
        transform.LookAt(target.transform);

        // move the enemy in the direction of its forward vector by some speed, mutlipled by Delta Time
        // so its the same speed on all mobile devices
        transform.position += (transform.forward * actor_speed) * Time.deltaTime;

        // if the enemy is close to the player
        // the enemy has hit the player
        if(Vector3.Distance(target.transform.position, transform.position) < 0.8f)
        {
            // reduce the players health
            target.GetComponent<Player>().NegateHealth();

            // Remove the enemy from the game world
            Destroy(this.gameObject);
        }
    }
}
