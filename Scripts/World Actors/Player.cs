﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

// Extends Actor: Main Player Controller Class
public class Player : Actor
{
    //
    // The Object that is hit by the ray, and the object that stores the audio content
    private GameObject hitObject;
    private GameObject audioLibrary;
    //

    // Game Mode: This handles all the game events like Score, Timer, Timer Negation, etc
    private GameMode GM;
    //

    //
    [Header("Particle Effects In The Game")]
    public GameObject explosionParticleFX;
    public GameObject fireballParticleFX;
    //

    // List of Projectile
    private List<GameObject> Projectiles = new List<GameObject>();
    //

    //
    [Header("Health UI Graphic")]
    public Text health;
    //

    // True if an object has been hit
    private bool Hit;
    //

    // Distance in which the ray can travel
    [Header("Length of the Raycast (How far can the player detect enemies)")]
    public int RayDistance;
    //

    public void Start()
    {
        // Get the Audio Library
        audioLibrary = GameObject.Find("AudioLibrary");

        InitialiseActor(0f, 50, 0); // Initialise this actor
        SetDamage(10); // the player can take 10 damage

        // Initialise our game mode
        GM = new GameMode();

        // Start the coroutine from the GameMode with Counts-down
        StartCoroutine(GM.CountdownCo());
    }

    // This function casts the ray from the cameras position along the cameras forward vector and ends depending on the distance.
    public void Update()
    {
        // Update the game events
        GM.UpdateGame();

        // initialise player health if its not null
        // Not done in the start because the health will be changed alot during gameplay
        if (health != null)
        {
            // Set the players health 
            health.text = actor_health.ToString();
        }

        // Raycast Hit Object
        RaycastHit hit;

        // If the left mouse button is down (Firing)
        if (Input.GetMouseButtonDown(0))
        {
            // Cast a ray along the cameras forward vector
            if (Physics.Raycast(transform.position, transform.forward, out hit, RayDistance))
            {
                // we have hit an object
                Hit = true;

                // get the hit object
                hitObject = hit.collider.gameObject;

                // if the hit object is a enemy (could use TAG instead)
                if (hitObject.name == "Enemy(Clone)")
                {
                    // overlay red colour when the enemy is hit 
                    hitObject.GetComponent<Renderer>().material.color = Color.red;

                    // Only fire projectiles if we detect an enemy
                    Projectiles.Add(Instantiate(fireballParticleFX, transform.position, Quaternion.identity));

                    // Play the shoot sound from the sound library
                    audioLibrary.GetComponent<SoundLibrary>().SHOOT.Play();

                    // decrease the enemys health
                    hitObject.GetComponent<Enemy>().NegateHealth();

                    // if the enemys health is less then or equal to 0
                    // then Spawn the explosion particle effect and destroy the enemy
                    if (hitObject.GetComponent<Enemy>().GetHealth() <= 0)
                    {
                        // Spawn Explosion
                        Instantiate(explosionParticleFX, hitObject.transform.position, Quaternion.identity);

                        // Destroy the enemy
                        Destroy(hitObject);

                        // Play the explosion sound from the sound library
                        audioLibrary.GetComponent<SoundLibrary>().EXPLOSION.Play();

                        // Increase the players score
                        GM.IncreaseScore();
                    }
                }
                // if we detect a pickup
                else if(hitObject.name == "HealthPowerup(Clone)")
                {
                    // play the pickup sound effect from the sound library
                    audioLibrary.GetComponent<SoundLibrary>().POWERUP.Play();

                    // Increase the time
                    GM.IncreaseTimeLeft();

                    // Destroy the pickup
                    Destroy(hitObject);
                }
            }
            else // hit is false if the ray doesnt detect anything
                Hit = false;
        }
        else
        {
            // if the hit object is not null and not hit
            if(hitObject != null)
            {
                // set the material to the default
                hitObject.GetComponent<Renderer>().material.color = Color.white;
            }
        }

        // If the players health is 0, then go to the game over screen
        if(GetHealth() <= 0)
        {
            SceneManager.LoadScene("GameOverScreen");
        }
    }
}
