﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Handles the spawning, fireing and physics of a projectile

// Important Note:
// --------------------------------------------
// This is assigned to the Projectile Prefab
// --------------------------------------------

public class Projectile : Actor
{
    // the main player camera
    private GameObject mainCam;

    void Start()
    {
        // get the main camera from the game world
        mainCam = GameObject.Find("Main Camera");

        // Initialise the actor
        InitialiseActor(10f, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        // fire the projectile along the cameras forward vector
        transform.Translate((mainCam.transform.forward * actor_speed) * Time.deltaTime);
    }

    void OnCollisionEnter(Collision collision)
    {
        //Check for a match with the specified name on any GameObject that collides with your GameObject
        if (collision.gameObject.name == "Enemy(Clone)")
        {
            //If the GameObject's name matches the one you suggest, output this message in the console
            Destroy(this.gameObject);
        }
    }
}
