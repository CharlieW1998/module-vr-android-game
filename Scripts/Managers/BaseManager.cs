﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class BaseManager : MonoBehaviour
{
    public void GoToGameLevel()
    {
        SceneManager.LoadScene("GameLevel");
    }
}
